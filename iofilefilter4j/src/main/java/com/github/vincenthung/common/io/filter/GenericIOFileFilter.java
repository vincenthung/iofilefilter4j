package com.github.vincenthung.common.io.filter;

import java.io.File;

import org.apache.commons.io.filefilter.IOFileFilter;

public class GenericIOFileFilter implements IOFileFilter {

	protected IOFileFilter filter = null;

	public GenericIOFileFilter() {}

	public GenericIOFileFilter(IOFileFilter filter) {
		this.filter = filter;
	}

	@Override
	public boolean accept(File file) {
		return filter.accept(file);
	}

	@Override
	public boolean accept(File dir, String name) {
		return filter.accept(dir, name);
	}

	public IOFileFilter getFilter() {
		return filter;
	}

	public void setFilter(IOFileFilter filter) {
		this.filter = filter;
	}

}
