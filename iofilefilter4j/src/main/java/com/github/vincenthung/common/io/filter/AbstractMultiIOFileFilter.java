package com.github.vincenthung.common.io.filter;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.filefilter.IOFileFilter;

public abstract class AbstractMultiIOFileFilter {

	protected List<IOFileFilter> filters = new ArrayList<IOFileFilter>();

	public AbstractMultiIOFileFilter(IOFileFilter... filters) {
		if (filters != null)
			for (IOFileFilter filter : filters)
				this.add(filter);
	}

	public void add(IOFileFilter filter) {
		filters.add(filter);
	}

	public boolean isEmpty() {
		return filters == null || filters.isEmpty();
	}

}
