package com.github.vincenthung.common.io.filter;

import java.io.File;

import org.apache.commons.io.filefilter.IOFileFilter;

public class IOFileFilterOr extends AbstractMultiIOFileFilter implements IOFileFilter {

	public IOFileFilterOr(IOFileFilter... filters) {
		super(filters);
	}

	@Override
	public boolean accept(File file) {
		if (this.isEmpty())
			return false;
		for (IOFileFilter filter : filters) {
			if (filter.accept(file))
				return true;
		}
		return false;
	}

	@Override
	public boolean accept(File dir, String name) {
		if (this.isEmpty())
			return false;
		for (IOFileFilter filter : filters) {
			if (filter.accept(dir, name))
				return true;
		}
		return false;
	}
}
